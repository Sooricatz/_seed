#  Barebones WP Theme for Bedrock 

# Require

1. [Bedrock Wordpress Stack](http://roots.io/wordpress-stack/)
2. [ACF PRO 5](http://www.advancedcustomfields.com/pro/)

## Build

1. Compile /less/style.less under /style.css AND /style.min.css (used on prod)
2. Compress /js/script.js under /js/script.min.js

## Production checklist

### Modernizr features