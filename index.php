<?php get_header(); ?>

	<div class="inside">

		<div id="content">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class() ?> >

				<h1><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1>

				<div class="entry">
					<?php the_content(); ?>
				</div>

				<footer class="postmetadata">
					Posted in <?php the_category(', ') ?> | 
				</footer>

			</article>

		<?php endwhile; ?>

		<?php else : ?>

			<h1>Not Found</h1>

		<?php endif; ?>

		</div>
	</div>

<?php get_footer(); ?>
