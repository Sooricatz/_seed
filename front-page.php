<?php get_header(); ?>


<?php if( have_rows('carousel_home', 'option') ): ?>
<div id="home-carousel">

	<div class="carousel-wrapper">
		<?php
			$myCounter=0;
			while( have_rows('carousel_home', 'option') ): the_row();
			$myCounter++;
		?>
		<img src="<?php the_sub_field('carousel_home_image'); ?>" />
		<?php endwhile; ?>
	</div> 

	<?php if ($myCounter>1): ?>
	<div class="carousel-nav">
		<div class="previous-image"></div>
		<div class="next-image"></div>
	</div>
	<?endif; ?>

</div>
<?php endif; ?>



<?php get_footer(); ?>