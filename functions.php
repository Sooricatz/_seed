<?php
/**
 * Functions File
 * @Developer Guillaume Mutschler (guillaumemutschler.eu)
 *  
 */


//////////////////
// Custom Utils   
////////////////// 
//require_once('inc/Mobile_Detect.php');
//require_once('inc/custom-post-types.php');


load_theme_textdomain( wp_get_theme().'_td',  get_template_directory() . '/translations' );



//////////////////////
// Mobile Detection
//////////////////////
// global $detect;
// global $isMobile;
// global $isIpad;
// global $isTablet;
// global $isIE;

// $detect   = new Mobile_Detect();
// $isMobile = ( $detect->isMobile() ) ? true : false;
// $isIpad   = ( $detect->isIpad() )   ? true : false;
// $isTablet = ( $detect->isTablet() ) ? true : false;
// $isIE     = ( $detect->isIE() )     ? true : false
// ;
// function mobile_class() {

//     global $isMobile; 
//     global $isIpad; 
//     global $isTablet; 

//     if ($isMobile) { 
//         echo " mobile"; 
//     }

//     if ($isIpad || $isTablet) { 
//         echo " tablet"; 
//     }
// }




/////////////////
// Theme Setup   
/////////////////
add_action( 'after_setup_theme', 'my_theme_setup' );

if ( !function_exists( 'my_theme_setup' ) ):

    function my_theme_setup() {
        
        // Add support for menus
        register_nav_menu('main-nav', 'Main navigation');
        //register_nav_menu('footer-menu', 'Footer menu');

        // Add support for title tag
        add_theme_support( 'title-tag' );
    }
endif;




//////////////////
// Init Scripts   
//////////////////
function load_my_script(){

    if( !is_admin()) {

        // Move jQuery in the footer
        wp_deregister_script( 'jquery' );
        wp_enqueue_script( 'jquery',
            '/wp-includes/js/jquery/jquery.js',
            false,
            '1.9',
            true
        );

        // Load my script
        $scriptName = constant('WP_ENV') == 'production' ? 'script.min.js' : 'script.js';

        wp_register_script( 
            'my_script', 
            get_template_directory_uri() . '/js/' . $scriptName, 
            array( 'jquery' ),
            false,
            true
        );
        wp_enqueue_script( 'my_script' );


        // Load style
        $styleName = constant('WP_ENV') == 'production' ? 'style.min.css' : 'style.css';

        wp_register_style(
            'my_style',
            get_template_directory_uri() . '/' . $styleName
        );
        wp_enqueue_style( 'my_style' );

        wp_register_script( 
            'modernizr', 
            '//modernizr.com/downloads/modernizr-latest.js', 
            false,
            false,
            false
        );
        wp_enqueue_script( 'modernizr' );

        
    }

}
add_action('wp_enqueue_scripts', 'load_my_script', 20);


/////////////////////////////////
// HTML5 Reset initializations   
/////////////////////////////////
    
// Clean up the <head>
function remove_head_links() {
    remove_action('wp_head', 'rsd_link'); //Really Simple Discovery service: Used by 3rd party software posts (like phones wordpress app -> reactivate if needed)
    remove_action('wp_head', 'wlwmanifest_link'); //Used by Windows Live Writer
    remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
    remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
    remove_action( 'wp_head', 'index_rel_link' ); // index link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
    remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
    remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version

}
add_action('init', 'remove_head_links');
//remove_action('wp_head', 'wp_generator');

include_once('inc/modules.php');
?>