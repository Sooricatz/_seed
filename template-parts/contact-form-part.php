<?php

$contact_infos=get_option('contact_infos');
$theEmail = isset($_POST['message_email']) ?  $_POST['message_email'] : '';
$theMessage = isset($_POST['message_text']) ?  $_POST['message_text'] : '';
$theName = isset($_POST['message_name']) ?  $_POST['message_name'] : '';

if (isset($_POST['submitted'])) {

	if(!filter_var($theEmail, FILTER_VALIDATE_EMAIL)) {
		$errorMessage = '<p class="error">'. _x('Please provide a valid Email Address.', 'contact-form', wp_get_theme().'_td') .'</p>';
	} else {

		if(empty($theMessage) || empty($theName)){
			$errorMessage = '<p class="error">'._x('Please provide all required informations.', 'contact-form', wp_get_theme().'_td').'</p>';
		} else {
		
			require ('inc/PHPMailer/PHPMailerAutoload.php');

			//Create a new PHPMailer instance
			$mail = new PHPMailer();
			$mail->CharSet = 'UTF-8';
			$mail->isHTML(true);      
			//Set who the message is to be sent from
			$mail->setFrom('contactform@'.domain_url(), get_bloginfo('name').' Website');
			//Set an alternative reply-to address
			$mail->addReplyTo($theEmail, $theName);
			//Set who the message is to be sent to
			$mail->addAddress($contact_infos['contact_email-email']);
			//Set the subject line
			$mail->Subject = 'Neue Nachricht von ' . $theName. ' auf '. str_replace( 'http://', '', site_url('', 'http') );


			$bodyMessage .= '<p>' . nl2br($theMessage) . '</p>';
			$bodyMessage .= '<p><br><br>_________________</p><p><small><i><b>Contact Informations</b><br>';
			$bodyMessage .= 'Name: ' .  $theName . '<br>';
			$bodyMessage .= 'E-Mail: ' .  $theEmail . '</small><i></p>';

			$mail->Body = $bodyMessage;
			$mail->AltBody = $bodyMessage;


			//send the message, check for errors
			if (!$mail->send()) {
			    $errorMessage = '<p class="error">'.printf(_x('Message was not sent. Try Again. Alternatively you can write us an email at %s.', 'contact-form', wp_get_theme().'_td'), $contact_infos['contact_email-email']).'</p>';
			} else {
				$theEmail = $theMessage = $theName = $theName =$thePhone = NULL;
			    $successMessage = '<p class="success">'._x('Thanks! Your message has been sent. We will get back to you shortly', 'contact-form', wp_get_theme().'_td').'</p>';
			}
		}
	}
}
?>


<div id="contact-form">
  <form action="<?php the_permalink(); ?>" method="post">
  	<div id="message-container">

  		<?php
  		if (!empty($errorMessage)) {
  			echo $errorMessage;
  		} elseif (!empty($successMessage)) {
  			echo $successMessage;
  		}
  		?>

  	</div>

    <div class="control-group">
    	<label for="message_name"><input id="message_name" type="text" name="message_name" value="<?php echo esc_attr($theName); ?>" placeholder="<?php _e('Name',wp_get_theme().'_td')?>"></label>
    	<label for="message_email"><input  id="message_email" type="text" name="message_email" value="<?php echo esc_attr($theEmail); ?>" placeholder="<?php _e('Email',wp_get_theme().'_td'); ?>"></label>
	</div>

    <div class="control-group">
    	<label for="message_text"><textarea id="message_text" name="message_text" placeholder="<?php _ex('Message', 'contact-form', wp_get_theme().'_td');?>" rows=8><?php echo esc_attr($theMessage); ?></textarea></label>
	</div>
    
    <div class="control-group">
    	<input type="hidden" name="submitted" value="1">
    	<input type="submit" value="<?php _e('Submit',wp_get_theme().'_td'); ?>">
	</div>
  </form>
</div>