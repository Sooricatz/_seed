
	<!-- Windows 8 -->
	<meta name="application-name" content="<?php bloginfo('name'); ?>" /> 
	<meta name="msapplication-TileColor" content="" /> 
	<meta name="msapplication-TileImage" content="<?php bloginfo('template_directory'); ?>/images/logo-big.png" />
	<!-- Twitter -->
	<meta name="twitter:card" content="summary" />
	<meta property="twitter:site" content="<?php bloginfo('name'); ?>" />
	<meta name="twitter:title" content="<?php bloginfo('name'); ?>" />
	<meta name="twitter:description" content="<?php bloginfo('description'); ?>" />
	<meta name="twitter:url" content="<?php echo curPageURL(); ?>" />
	<meta name="twitter:image" content="<?php bloginfo('template_directory'); ?>/images/logo-big.png" />
	<!-- Facebook -->
	<meta property="og:title" content="<?php bloginfo('name'); ?>" />
	<meta property="og:description" content="<?php bloginfo('description'); ?>" />
	<meta property="og:url" content="<?php echo curPageURL(); ?>" />
	<meta property="og:image" content="<?php bloginfo('template_directory'); ?>/images/logo-big.png" />