<!DOCTYPE html>
<html <?php language_attributes(); ?>>


<head>
	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<!--[if IE ]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->

	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="<?php bloginfo('name') ?>">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	
	<link rel="icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico?v=1.0">
		
	<?php wp_head(); ?>
	<?php get_template_part('template-parts/graphtags'); ?>

	<!--[if lt IE 9]><script src="<?php bloginfo('template_directory'); ?>/js/utils/html5shiv.min.js"></script><![endif]-->
</head>

<body <?php body_class(); ?>>

	<header role="banner">
		<div class="wrapper">
			<a id="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="" rel="home"><span><?php bloginfo('name'); ?></span></a>
			<div id="main-nav" class="nav-menu group" role="navigation">
				<?php wp_nav_menu( array('theme_location' => 'main-menu') ); ?>
			</div>
			<div id="hamburger-menu"></div>
		</div>
	</header>

	<main role="main">
		<div class="wrapper">

