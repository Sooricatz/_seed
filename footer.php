
		</div><!-- main wrapper -->
	</main>
	<?php wp_reset_postdata(); ?>

	<div id="footer">
		<div class="inside">
	
		</div>
	</div><!-- #footer -->

	<?php wp_footer(); ?>

	<?php
	//load analytics if on prod server
	if (constant('WP_ENV') == 'production') {
		get_template_part('template-parts/analyticstracking');
	}
	?>

</body>

</html>
