<?php get_header(); ?>

<div class="inside">
	<div id="content">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<h1 class="pagetitle"><?php the_title(); ?></h1>
				
			<?php get_template_part('template-part/basic', 'post-part'); ?>
			
			<?php

			$args = array(
			    'post_type'      => 'page',
			    'posts_per_page' => -1,
			    'post_parent'    => $post->ID,
			    'order'          => 'ASC',
			    'orderby'        => 'menu_order'
			 );


			$parent = new WP_Query( $args );
			?>



			<?php if ($parent->have_posts()) : while ($parent->have_posts()) : $parent->the_post(); ?>
					
			<?php get_template_part('template-part/basic', 'post-part'); ?>

			<?php endwhile; endif; ?>
			
		<?php endwhile; endif; ?>

	</div>

</div>

<?php get_footer(); ?>
