// remap jQuery to $
(function($){
        
     var $window,
         $body;

        
    ////////
    // Init
    ////////
    function init() {

        // Init onResize handler
        $window.resize(function() {
            resizeSite();
        });

        // Modernizr
        if (!Modernizr.svg) {
            $('img[src$=".svg"]').each(function() {
                var imgURL = $(this).attr('src').replace('.svg','.png');
                $(this).attr('src', imgURL);
            });
        }
    }


    //////////////
    // Animations
    //////////////
/*    function animate() {

        requestAnimationFrame( animate );

        //Query DOM


        //Do operations


        //Upate DOM
    }
*/

   

    /////////////
    // Resize
    /////////////
    function resizeSite() {

        windowWidth  = $window.width();
        windowHeight = $window.height();
    }


    ////////////
    // load
    ////////////
    function onLoad() {

    }

    /////////////
    // onReady
    /////////////

    (function($){
        $(function() {

            //Define DOM Objects
            $window         = $(window);
            $body           = $('body');

            // Resize
            resizeSite();

            //Init and bind events
            init();

            // Load
            //$(window).on('load', onLoad);
        });
    })(jQuery);



    //////////////////////////////////////
    // UTILS: SNIPPETS AND JQUERY EXTENDS
    //////////////////////////////////////

    // Custom Easing Extends
    // $.extend($.easing,
    // {
    //     def: 'easeOutQuad',
    //     swing: function (x, t, b, c, d) {
    //         return $.easing[$.easing.def](x, t, b, c, d);
    //     },

    //     easeInQuad: function (x, t, b, c, d) {
    //         return c*(t/=d)*t + b;
    //     },
    //     easeOutQuad: function (x, t, b, c, d) {
    //         return -c *(t/=d)*(t-2) + b;
    //     },
    //     easeInOutQuad: function (x, t, b, c, d) {
    //         if ((t/=d/2) < 1) return c/2*t*t + b;
    //         return -c/2 * ((--t)*(t-2) - 1) + b;
    //     },
    //     easeInCubic: function (x, t, b, c, d) {
    //         return c*(t/=d)*t*t + b;
    //     },
    //     easeOutCubic: function (x, t, b, c, d) {
    //         return c*((t=t/d-1)*t*t + 1) + b;
    //     },
    //     easeInOutCubic: function (x, t, b, c, d) {
    //         if ((t/=d/2) < 1) return c/2*t*t*t + b;
    //         return c/2*((t-=2)*t*t + 2) + b;
    //     }
    // });


})(jQuery);
